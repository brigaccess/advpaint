unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, LCLType,
  Menus, ExtCtrls, Buttons, ComCtrls, StdCtrls, Grids, UTools, UFigures,
  Legal, UCoordinateTranslator, types, Math, UExUtils, UPropertiesEditor,
  UFormats, UXAPFormat, USVGFormat; { TODO: Loading modules properly }

type

  { TMainForm }

  TMainForm = class(TForm)
    ColorDialog: TColorDialog;
    DrawGridPalette: TDrawGrid;
    MainMenu: TMainMenu;
    MenuFileNew: TMenuItem;
    MenuFileOpen: TMenuItem;
    MenuFileSave: TMenuItem;
    MenuFileSaveAs: TMenuItem;
    MenuFileExport: TMenuItem;
    MenuFileSeparator: TMenuItem;
    MenuFileImport: TMenuItem;
    MenuFileImpExpSeparator: TMenuItem;
    MenuItemUndo: TMenuItem;
    MenuItemRedo: TMenuItem;
    MenuItemHelpSeparator1: TMenuItem;
    MenuItemLegal: TMenuItem;
    MenuItemHelp: TMenuItem;
    MenuItemQuit: TMenuItem;
    MenuItemAbout: TMenuItem;
    MenuItemEdit: TMenuItem;
    MenuItemFile: TMenuItem;
    OpenDialog: TOpenDialog;
    ImportDialog: TOpenDialog;
    PaintBox: TPaintBox;
    PalettePanel: TPanel;
    PropsPanel: TPanel;
    SaveDialog: TSaveDialog;
    ExportDialog: TSaveDialog;
    ScrollBarHoriz: TScrollBar;
    ScrollBarVert: TScrollBar;
    ShapeFG: TShape;
    ShapeBG: TShape;
    ShapeFGol: TShape;
    ShapeBGol: TShape;
    SpeedButtonSwap: TSpeedButton;
    SpeedButtonCenter: TSpeedButton;
    SpeedButtonDefault: TSpeedButton;
    StatusBar: TStatusBar;
    ToolsPanel: TPanel;

    procedure DrawGridPaletteDblClick(Sender: TObject);
    procedure DrawGridPaletteDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure DrawGridPaletteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DrawGridPaletteSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);

    procedure FormCreate(Sender: TObject);

    procedure FormKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure MenuFileExportClick(Sender: TObject);
    procedure MenuFileImportClick(Sender: TObject);
    procedure MenuFileNewClick(Sender: TObject);
    procedure MenuFileOpenClick(Sender: TObject);
    procedure MenuFileSaveAsClick(Sender: TObject);
    procedure MenuFileSaveClick(Sender: TObject);

    procedure MenuItemAboutClick(Sender: TObject);
    procedure MenuItemFileClick(Sender: TObject);
    procedure MenuItemLegalClick(Sender: TObject);
    procedure MenuItemQuitClick(Sender: TObject);
    procedure MenuItemRedoClick(Sender: TObject);
    procedure MenuItemUndoClick(Sender: TObject);

    procedure PaintBoxClick(Sender: TObject);
    procedure PaintBoxDblClick(Sender: TObject);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure PaintBoxMouseLeave(Sender: TObject);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure PaintBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: boolean);
    procedure PaintBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: boolean);
    procedure PaintBoxPaint(Sender: TObject);

    procedure ScrollBarsPBChange(Sender: TObject);

    procedure ShapeBGMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ShapeFGMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

    procedure SpeedButtonCenterClick(Sender: TObject);
    procedure SpeedButtonDefaultClick(Sender: TObject);
    procedure SpeedButtonSwapClick(Sender: TObject);

    procedure StatusBarResize(Sender: TObject);

    procedure ToolButtonClick(Sender: TObject);

    function GetCanvasRectangle(): TRect;

  private
    DefaultPen: TPen;
    DefaultBrush: TBrush;

    Palette: array[0..255] of TColor;
    LatestClicked: TPoint;
    LatestClickedCols: Integer;

    CurrentForeground, CurrentBackground: TColor;

    ScrollbarsUpdating: Boolean;
    procedure SetBackground(AValue: TColor);
    procedure SetForeground(AValue: TColor);
    property Background: TColor read CurrentBackground write SetBackground;
    property Foreground: TColor read CurrentForeground write SetForeground;

    procedure SwapColors();
    procedure DefaultColors();

    function GetColorNum(x, y, columns: Integer): Integer;
    function GetColorOfCell(x, y, columns: Integer): TColor;
    function GetColorOfCell(cell: TPoint; columns: Integer): TColor;

    procedure NewScene();
    procedure Open();
    procedure Save();
    procedure SaveAs();
    procedure FileExport();
    procedure Import();
    procedure UpdateCaption();
    function AskForSave(): boolean;

    const
      DEBUG_BOUNDS = False;
  public
    { public declarations }
  end;

var
  MainForm: TMainForm;
  LatestSave: integer;
  FilePath: string = '';

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.MenuItemQuitClick(Sender: TObject);
begin
  if AskForSave() then
    Application.Terminate;
end;

procedure TMainForm.MenuItemRedoClick(Sender: TObject);
begin
  if UTools.CurrentTool.IsActive() then exit();
  UFigures.Redo();
  PaintBox.Invalidate;
end;

procedure TMainForm.MenuItemUndoClick(Sender: TObject);
begin
  if UTools.CurrentTool.IsActive() then exit();
  UFigures.Undo();
  PaintBox.Invalidate;
end;

procedure TMainForm.PaintBoxClick(Sender: TObject);
begin
  with ScreenToClient(Mouse.CursorPos) do begin
    UTools.CurrentTool.OnCanvasClick(
      ScreenToWorld(Point(X - PaintBox.Left, Y - PaintBox.Top))
    );
  end;
end;

procedure TMainForm.PaintBoxDblClick(Sender: TObject);
begin
  UTools.CurrentTool.OnOperationEnd();
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  ToolButton: TSpeedButton;
  i, j: integer;
  t: TRectangle; // TODO Get rid of debug entity
  red, green, blue: byte;
begin
  DefaultPen := TPen.Create;
  DefaultBrush := TBrush.Create;

  UTools.Editor := TFigureEditor.Create(PropsPanel, PaintBox);
  DefaultColors();

  // Registering buttons
  for i := 0 to High(UTools.Tools) do
  begin
    ToolButton := TSpeedButton.Create(Self);
    ToolButton.Parent := ToolsPanel;

    ToolButton.Width := 24;
    ToolButton.Height := 24;

    ToolButton.Left := 8;
    if (i mod 2 <> 0) then
      ToolButton.Left := ToolButton.Left + ToolButton.Width;
    ToolButton.Top := 8 + (i div 2) * ToolButton.Height;

    ToolButton.Tag := i;
    ToolButton.OnClick := @ToolButtonClick;
    ToolButton.Name := 'SpeedButtonTool' + IntToStr(i);
    ToolButton.GroupIndex := 1;

    ToolButton.Flat := True;
    ToolButton.Glyph.Assign(UTools.Tools[i].getIcon());

    if i = 0 then begin
      ToolButton.Down := True;
      ToolButton.OnClick(ToolButton);
    end;

  end;

  for i := 0 to 15 do begin
    red := (i div 4) * 85;
    green := (i mod 4) * 85;
    for j := 0 to 3 do begin
      blue := j * 85;
      Palette[(i * 4) + j] := RGBToColor(red, green, blue);
    end;
  end;
end;

procedure TMainForm.DrawGridPaletteMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  cell: TPoint;
begin
  cell := DrawGridPalette.MouseToCell(Point(X, Y));

  if (cell.x > -1) and (cell.y > -1) then begin
    if Button = Button.mbLeft then
      SetForeground(GetColorOfCell(cell, DrawGridPalette.ColCount))
    else if Button = Button.mbRight then
      SetBackground(GetColorOfCell(cell, DrawGridPalette.ColCount));
  end;
end;

procedure TMainForm.DrawGridPaletteSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
begin
  CanSelect := False;
  LatestClicked := Point(aCol, aRow);
  LatestClickedCols := DrawGridPalette.ColCount;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose := AskForSave();
end;

procedure TMainForm.DrawGridPaletteDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
begin
  DrawGridPalette.Canvas.Pen.Color := Palette[aRow * DrawGridPalette.ColCount + aCol];
  DrawGridPalette.Canvas.Brush.Color := Palette[aRow * DrawGridPalette.ColCount + aCol];
  DrawGridPalette.Canvas.Rectangle(aRect);
end;

procedure TMainForm.DrawGridPaletteDblClick(Sender: TObject);
begin
  if (LatestClicked.X > -1) and (LatestClicked.Y > -1) then begin
    ColorDialog.Color := GetColorOfCell(LatestClicked, LatestClickedCols);
    if ColorDialog.Execute then begin
      Palette[
        GetColorNum(LatestClicked.x, LatestClicked.y, LatestClickedCols)
      ] := ColorDialog.Color;

      DrawGridPalette.Invalidate;
    end;
  end;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  UTools.CurrentTool.OnControlKeysStateChange(Shift);
end;

procedure TMainForm.FormKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
      UTools.CurrentTool.OnOperationEnd();
    else
      UTools.CurrentTool.OnControlKeysStateChange(Shift)
  end;
end;

procedure TMainForm.MenuFileExportClick(Sender: TObject);
begin
  FileExport()
end;

procedure TMainForm.MenuFileImportClick(Sender: TObject);
begin
  Import()
end;

procedure TMainForm.MenuFileNewClick(Sender: TObject);
begin
  NewScene();
end;

procedure TMainForm.MenuFileOpenClick(Sender: TObject);
begin
  Open();
end;

procedure TMainForm.MenuFileSaveAsClick(Sender: TObject);
begin
  SaveAs();
end;

procedure TMainForm.MenuFileSaveClick(Sender: TObject);
begin
  Save();
end;

procedure TMainForm.PaintBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: boolean);
begin
  if not UTools.CurrentTool.IsActive() then
  begin
    UCoordinateTranslator.ZoomOut(MousePos, GetCanvasRectangle());
    PaintBox.Invalidate;
  end;
end;

procedure TMainForm.PaintBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: boolean);
begin
  if not UTools.CurrentTool.IsActive() then
  begin
    UCoordinateTranslator.ZoomIn(MousePos, GetCanvasRectangle());
    PaintBox.Invalidate;
  end;
end;

procedure TMainForm.MenuItemAboutClick(Sender: TObject);
begin
  ShowMessage('Basic graphics editor' + #13#10 +
    'Written by Brovtsin Igor, B8103a-1, 2015' + #13#10 + #13#10 +
    'Version: w5 "Following the train"'
    );
end;

procedure TMainForm.MenuItemFileClick(Sender: TObject);
begin
  if Length(UFormats.GetExportersFilters()) = 0 then begin
    MenuFileExport.Enabled := False;
  end;
  if Length(UFormats.GetImportersFilters()) = 0 then begin
    MenuFileImport.Enabled := False;
  end;
end;

procedure TMainForm.MenuItemLegalClick(Sender: TObject);
begin
  Legal.LegalForm.ShowModal();
end;

procedure TMainForm.PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  UTools.CurrentTool.OnStartDrawing(Point(x, y));
  UTools.CurrentTool.OnStartDrawing(ScreenToWorld(Point(x, y)),
    Foreground, Background);

  PaintBox.Invalidate;
end;

procedure TMainForm.PaintBoxMouseLeave(Sender: TObject);
begin
  StatusBar.Panels.Items[1].Text := '';
end;

procedure TMainForm.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
var
  wp: TDblPoint;
begin
  wp := ScreenToWorld(Point(X, Y));
  StatusBar.Panels.Items[1].Text := Format('(%.2f, %.2f)', [wp.x, wp.y]);

  UTools.CurrentTool.OnMove(Point(x, y), Shift);
  UTools.CurrentTool.OnMove(ScreenToWorld(Point(x, y)), Shift);

  PaintBox.Invalidate;
end;

procedure TMainForm.PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  UTools.CurrentTool.OnFinishedDrawing(Point(x, y));
  UTools.CurrentTool.OnFinishedDrawing(ScreenToWorld(Point(x, y)));

  PaintBox.Invalidate;
end;

procedure TMainForm.PaintBoxPaint(Sender: TObject);
var
  i: integer;
  worldBounds: TDblRectEx;
  worldBoundsScreen: TRect;
begin
  PaintBox.Canvas.Brush.Color := clWhite;
  PaintBox.Canvas.FillRect(0, 0, PaintBox.Width, PaintBox.Height);
  for i := 0 to High(UFigures.Figures) do
  begin
    UFigures.Figures[i].Draw(PaintBox.Canvas);
    PaintBox.Canvas.Pen := DefaultPen;
    PaintBox.Canvas.Brush := DefaultBrush;
  end;

  UTools.CurrentTool.DrawHelpers(PaintBox.Canvas);

  worldBounds := UFigures.GetMultipleBounds(UFigures.Figures);
  worldBoundsScreen := WorldRectToScreenRect(worldBounds);

  ShapeFGol.Brush.Color := Foreground;
  ShapeBGol.Brush.Color := Background;

  with PaintBox.Canvas do begin
    if DEBUG_BOUNDS then begin
      Pen.Color := clBlue;
      Pen.Style := psDash;
      Brush.Style := bsClear;

      Rectangle(worldBoundsScreen);

      Pen.Color := clRed;
      Pen.Style := psSolid;

      Line(PaintBox.Width div 2, 0, PaintBox.Width div 2, PaintBox.Height);
      Line(0, PaintBox.Height div 2, PaintBox.Width, PaintBox.Height div 2);

      Brush := DefaultBrush;
      Pen := DefaultPen;
    end;
  end;

  ScrollbarsUpdating := True;
  with worldBoundsScreen do begin
     if (BottomRight.X < PaintBox.Width) and (TopLeft.X >= 0) then begin
       ScrollBarHoriz.Visible := False;
     end
     else if (TopLeft.X <> BottomRight.X) or (TopLeft.Y <> BottomRight.Y) then
     begin
       if not ScrollBarHoriz.Visible then ScrollBarHoriz.Visible := True;
       ScrollBarHoriz.Max := Max(
        -Floor(XOffset),
        Max(TopLeft.X, BottomRight.X) - Floor(XOffset)
       );
       ScrollBarHoriz.Min := Min(
        Floor(-XOffset),
        Min(TopLeft.X, BottomRight.X) - Floor(XOffset)
       );
       ScrollBarHoriz.PageSize := PaintBox.Width;
       ScrollBarHoriz.Position := Round(-XOffset);
     end;

     if (BottomRight.Y < PaintBox.Height) and (TopLeft.Y >= 0) then begin
       ScrollBarVert.Visible := False;
     end
     else if (TopLeft.X <> BottomRight.X) or (TopLeft.Y <> BottomRight.Y) then
     begin
       if not ScrollBarVert.Visible then ScrollBarVert.Visible := True;
       ScrollBarVert.Max := Max(
        PaintBox.Height - Floor(YOffset),
        Max(TopLeft.Y, BottomRight.Y) - Floor(YOffset)
       );
       ScrollBarVert.Min := Min(
        Floor(-YOffset),
        Min(TopLeft.Y, BottomRight.Y) - Floor(YOffset)
       );
       ScrollBarVert.PageSize := PaintBox.Height;
       ScrollBarVert.Position := Round(-YOffset);
     end;
  end;
  ScrollbarsUpdating := False;

  MenuItemUndo.Enabled := UFigures.HistoryStep > 0;
  MenuItemRedo.Enabled := UFigures.HistoryStep < UFigures.History.Count - 1;

  UpdateCaption();

end;

procedure TMainForm.ScrollBarsPBChange(Sender: TObject);
begin
  with (Sender as TScrollBar) do begin
  if not ScrollbarsUpdating then begin
    { Fixes visual annoyance caused by bug that allows you to move the
    scrollbar even further by just pressing "<" and ">" buttons. Even though
    it could be an intended behaviour, it looks (and feels) really awful }
    if (PageSize + Position > Max) then exit();

    if Tag = 1 then
      UCoordinateTranslator.XOffset := -Position
    else
      UCoordinateTranslator.YOffset := -Position;
    PaintBox.Invalidate;
  end;
  end;
end;

procedure TMainForm.ShapeBGMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button <> Button.mbLeft then exit();
  ColorDialog.Color := Background;
  if ColorDialog.Execute then
    Background := ColorDialog.Color;
end;

procedure TMainForm.ShapeFGMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button <> Button.mbLeft then exit();
  ColorDialog.Color := Foreground;
  if ColorDialog.Execute then
    Foreground := ColorDialog.Color;
end;

procedure TMainForm.SpeedButtonCenterClick(Sender: TObject);
var
  worldBounds: TDblRectEx;
begin
  if Length(UFigures.Figures) < 1 then exit();

  worldBounds := UFigures.GetWorldBounds();
  with worldBounds do begin
    UCoordinateTranslator.SetViewport(
      TopLeft.X, TopLeft.Y, BottomRight.X, BottomRight.Y, GetCanvasRectangle()
    );
    UCoordinateTranslator.CenterOn(
      DblPoint(
       ((BottomRight.X - TopLeft.X) / 2) + TopLeft.X,
       ((BottomRight.Y - TopLeft.Y) / 2) + TopLeft.Y
      ), GetCanvasRectangle()
    );
  end;
  PaintBox.Invalidate;
end;

procedure TMainForm.SpeedButtonDefaultClick(Sender: TObject);
begin
  DefaultColors();
end;

procedure TMainForm.SpeedButtonSwapClick(Sender: TObject);
begin
  SwapColors();
end;

procedure TMainForm.StatusBarResize(Sender: TObject);
begin
  StatusBar.Panels.Items[0].Width := (StatusBar.Width - 100);
end;

procedure TMainForm.ToolButtonClick(Sender: TObject);
begin
  if UTools.CurrentTool <> Nil then
    UTools.CurrentTool.OnToolChange();
  UTools.CurrentTool := UTools.Tools[(Sender as TSpeedButton).Tag];
  UTools.CurrentTool.Init();
end;

function TMainForm.GetCanvasRectangle: TRect;
begin
  exit(Rect(0, 0, PaintBox.Width, PaintBox.Height));
end;

function TMainForm.GetColorOfCell(x, y, columns: Integer): TColor;
begin
  Result := Palette[GetColorNum(x, y, columns)];
end;

function TMainForm.GetColorOfCell(cell: TPoint; columns: Integer): TColor;
begin
  Result := GetColorOfCell(cell.x, cell.y, columns);
end;

procedure TMainForm.NewScene;
begin
  if not AskForSave() then exit();
  UFigures.ClearScene();
  UFigures.ClearHistory();
  UTools.Editor.Cleanup();
  FilePath := '';
  PaintBox.Invalidate;
end;

procedure TMainForm.Open;
begin
  if not AskForSave() then exit();
  OpenDialog.Filter := UFormats.GetFullCompatFilters();
  PaintBox.Enabled := False;
  if OpenDialog.Execute then begin
    UFormats.ImportFile(OpenDialog.FileName);
    FilePath := OpenDialog.FileName;
    Paintbox.Invalidate;
    UpdateCaption();
  end;
  PaintBox.Enabled := True;
end;

procedure TMainForm.Save;
begin
  if FilePath = '' then begin
    SaveAs();
  end
  else begin
    UFormats.ExportFile(FilePath, Figures, PaintBox.Canvas);
    UFigures.LatestSave := UFigures.HistoryStep;
    UpdateCaption();
  end;
end;

procedure TMainForm.SaveAs;
begin
  SaveDialog.Filter := UFormats.GetFullCompatFilters();
  PaintBox.Enabled := False;
  if SaveDialog.Execute then begin
    UFormats.ExportFile(SaveDialog.FileName, Figures, PaintBox.Canvas);
    FilePath := SaveDialog.FileName;
    UFigures.LatestSave := UFigures.HistoryStep;
    UpdateCaption();
  end;
  PaintBox.Enabled := True;
end;

procedure TMainForm.FileExport;
begin
  ExportDialog.Filter := UFormats.GetExportersFilters();
  if ExportDialog.Execute then begin
    UFormats.ExportFile(ExportDialog.FileName, Figures, PaintBox.Canvas);
  end;
end;

procedure TMainForm.Import;
begin
  if not AskForSave() then exit();
  ImportDialog.Filter := UFormats.GetExportersFilters();
  if ImportDialog.Execute then begin
    UFormats.ImportFile(ImportDialog.FileName);
    Paintbox.Invalidate;
  end;
end;

procedure TMainForm.UpdateCaption;
var
  newCaption: String;
begin
  newCaption := '';
  if FilePath = '' then
    newCaption := 'Untitled'
  else
    newCaption := ExtractFileName(FilePath);

  if UFigures.Changed() then
    newCaption += '*';

  MainForm.Caption := newCaption + ' - Paint';
end;

function TMainForm.AskForSave(): boolean;
var
  s: string;
begin
  if not UFigures.Changed() then exit(True);
  s := 'Do you want to save ';
  if FilePath = '' then s += 'Untitled?'
  else s += ExtractFileName(FilePath) + '?';

  case MessageDlg('Question', s, mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
     mrYes: begin
       Save();
     end;
     mrCancel: begin
       exit(False);
     end;
  end;
  exit(True);
end;

procedure TMainForm.SetBackground(AValue: TColor);
begin
  CurrentBackground := AValue;
  ShapeBG.Brush.Color := CurrentBackground;
  UTools.Editor.ForceChange('BGColor', AValue);
end;

procedure TMainForm.SetForeground(AValue: TColor);
begin
  CurrentForeground := AValue;
  ShapeFG.Brush.Color := CurrentForeground;
  UTools.Editor.ForceChange('FGColor', AValue);
end;

procedure TMainForm.SwapColors();
var
  buf: TColor;
begin
  buf := Foreground;
  Foreground := Background;
  Background := buf;
end;

procedure TMainForm.DefaultColors();
begin
  Foreground := clBlack;
  Background := clWhite;
end;

function TMainForm.GetColorNum(x, y, columns: Integer): Integer;
begin
  Result := (y * columns) + x;
end;

end.
