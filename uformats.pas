unit UFormats;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UFigures, UExUtils, Graphics;

type

  { TExporter }

  TExporter = class(TObject)
  public
    procedure Save(Filename: string; Figures: TFigures; Canvas: TCanvas); virtual;
  end;

  { TImporter }

  TImporter = class(TObject)
  public
    procedure Load(Filename: string); virtual;
  end;

  TRegisteredExporter = record
    filter: String;
    exporter: TExporter;
  end;
  TSaverRegistry = array of TRegisteredExporter;

  TRegisteredImporter = record
    filter: String;
    importer: TImporter;
  end;
  TImporterRegistry = array of TRegisteredImporter;

  procedure ImportFile(Filename: String);
  procedure ExportFile(Filename: String; Figures: TFigures; Canvas: TCanvas);
  function GetImportersFilters(): String;
  function GetExportersFilters(): String;
  function GetFullCompatFilters(): String;
  procedure RegisterImporter(AFilter: String; AImporter: TImporter);
  procedure RegisterExporter(AFilter: String; AExporter: TExporter);
  procedure UpdateCompat(AFilter: String);

implementation
var
  ImporterRegistry: TImporterRegistry;
  ExporterRegistry: TSaverRegistry;
  FullySupported: TStringList;

{ TODO: Handle multiple import/export formats that use same extension }
procedure ImportFile(Filename: String);
var
  s: TRegisteredImporter;
  ext: String;
begin
  { TODO: Import based on ability of importers to open the file, not by extension }
  ext := '*' + ExtractFileExt(Filename);
  for s in ImporterRegistry do begin
    if pos(ext, s.filter) > 0 then begin
      UFigures.ClearScene();
      s.importer.Load(Filename);
      UFigures.ClearHistory();
      exit();
    end;
  end;
  raise Exception.Create('Unknown format for file ' + ExtractFileName(Filename));
end;

procedure ExportFile(Filename: String; Figures: TFigures; Canvas: TCanvas);
var
  s: TRegisteredExporter;
  ext: String;
begin
  ext := '*' + ExtractFileExt(Filename);
  for s in ExporterRegistry do begin
    if pos(ext, s.filter) > 0 then begin
      s.exporter.Save(Filename, Figures, Canvas);
      exit();
    end;
  end;
  raise Exception.Create('Unknown format for file ' + ExtractFileName(Filename));
end;

function GetFullCompatFilters: String;
var
  item: String;
begin
  Result := '';
  for item in FullySupported do begin
    if Length(Result) > 0 then Result += '|';
    Result += item;
  end;
end;

procedure RegisterImporter(AFilter: String; AImporter: TImporter);
begin
  SetLength(ImporterRegistry, Length(ImporterRegistry) + 1);
  with ImporterRegistry[High(ImporterRegistry)] do begin
    filter := AFilter;
    importer := AImporter;
  end;
  UpdateCompat(AFilter);
end;

procedure RegisterExporter(AFilter: String; AExporter: TExporter);
begin
  SetLength(ExporterRegistry, Length(ExporterRegistry) + 1);
  with ExporterRegistry[High(ExporterRegistry)] do begin
    filter := AFilter;
    exporter := AExporter;
  end;
  UpdateCompat(AFilter);
end;

procedure UpdateCompat(AFilter: String);
var
  s: String;
  ri: TRegisteredImporter;
  re: TRegisteredExporter;
begin
  if FullySupported.IndexOf(AFilter) = -1 then begin
     for ri in ImporterRegistry do begin
       if ri.filter <> AFilter then continue;
       for re in ExporterRegistry do begin
         if re.filter <> AFilter then continue;
         FullySupported.Add(AFilter);
       end;
     end;
  end;
end;

function GetImportersFilters: String;
var
  i: integer;
begin
  Result := '';
  for i := 0 to High(ImporterRegistry) do begin
    if FullySupported.IndexOf(ImporterRegistry[i].filter) <> -1 then continue;
    Result += ImporterRegistry[i].filter;
    if i < High(ImporterRegistry) - 1 then begin
      Result += '|';
    end;
  end;
end;

function GetExportersFilters: String;
var
  i: integer;
begin
  Result := '';
  for i := 0 to High(ExporterRegistry) do begin
    if FullySupported.IndexOf(ExporterRegistry[i].filter) <> -1 then continue;
    Result += ExporterRegistry[i].filter;
    if i < High(ExporterRegistry) - 1 then begin
      Result += '|';
    end;
  end;
end;

{ TImporter }

procedure TImporter.Load(Filename: string);
begin

end;

{ TExporter }

procedure TExporter.Save(Filename: string; Figures: TFigures; Canvas: TCanvas);
begin

end;

initialization
FullySupported := TStringList.Create;

end.

