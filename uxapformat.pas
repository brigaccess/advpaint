unit UXAPFormat;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UFormats, UFigures, Graphics;

type
  { TXAPImporter }

  TXAPImporter = class(TImporter)
  public
    procedure Load(Filename: string); override;
  const
    VERSION = '1.0';
  end;

  { TXAPExporter }

  TXAPExporter = class(TExporter)
  public
    procedure Save(Filename: string; Figures: TFigures; Canvas: TCanvas); override;
  const
    VERSION = '1.0';
  end;

  const XAPFilter = 'XAP Files|*.xap';

implementation

{ TXAPExporter }

procedure TXAPExporter.Save(Filename: string; Figures: TFigures; Canvas: TCanvas);
var
  f: Text;
begin
  AssignFile(f, Filename);
  Rewrite(f);
  Writeln(f, 'XAP');
  Writeln(f, VERSION);
  Write(f, UFigures.PresentScene());
  Close(f);
end;

{ TXAPImporter }

procedure TXAPImporter.Load(Filename: string);
var
  f: Text;
  ver: string;
begin
  AssignFile(f, Filename);
  Reset(f);
  ReadLn(f, ver);
  if ver = 'XAP' then begin
    ReadLn(f, ver);
    if ver = VERSION then begin
      while not EOF(f) do begin
        ReadLn(f, ver);
        UFigures.BuildFigure(ver);
      end;
      Close(f);
    end
    else begin
      // TODO Legacy support
      raise Exception.Create(
        Format('Unsupported XAP version: I know %s, but was given %s', [VERSION, ver]
      ));
    end
  end;
end;

initialization
UFormats.RegisterImporter(XAPFilter, TXAPImporter.Create());
UFormats.RegisterExporter(XAPFilter, TXAPExporter.Create());

end.

