unit UTools;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UFigures, Graphics, Dialogs, Math, UCoordinateTranslator,
  UExUtils, LCLIntf, LCLType, UPropertiesEditor;

type
  { TTool }

  TTool = class(TObject)
  public
    constructor Create(AIconName: string); virtual;
    procedure Init(); virtual;
    procedure OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor); virtual;
      overload;
    procedure OnStartDrawing(Pos: TPoint); virtual; overload;
    procedure OnMove(Pos: TDblPoint; Shift: TShiftState); virtual; overload;
    procedure OnMove(Pos: TPoint; Shift: TShiftState); virtual; overload;
    procedure OnFinishedDrawing(Pos: TDblPoint); virtual; overload;
    procedure OnFinishedDrawing(Pos: TPoint); virtual; overload;
    procedure OnCanvasClick(Pos: TDblPoint); virtual;
    procedure OnCanvasDblClick(Pos: TDblPoint); virtual;
    procedure OnToolChange(); virtual;
    procedure OnControlKeysStateChange(Shift: TShiftState); virtual;
    procedure OnOperationEnd(); virtual; // Handle 'Enter' keypress
    procedure DrawHelpers(ACanvas: TCanvas); virtual;

    function GetIcon(): TBitmap; virtual;
    function IsActive(): boolean; virtual;
  private
    Active: boolean;
    IconName: string;
  end;


  { TDrawingTool }
  { TODO: Rename }
  TDrawingTool = class(TTool)
  public
    procedure OnFinishedDrawing(Pos: TDblPoint); override; overload;
  end;

  { Hack: Subclasses of TLinearFigure can be assigned so that we wouldn't repeat
  the same code for different tools }
  TLinearFigureClass = class of TLinearFigure;

  { TLinearTool }

  TLinearTool = class(TDrawingTool)
  public
    constructor Create(ALinFigureClass: TLinearFigureClass; AIconName: string);
    procedure Init(); override;
    procedure OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor); override;
    procedure OnMove(Pos: TDblPoint; Shift: TShiftState); override;
  private
    ResultingFigure: TLinearFigureClass;
    CurrentFigure: TLinearFigure;
    Center: TDblPoint;
  end;

  { TAngledLinearTool }

  TAngledLinearTool = class(TLinearTool)
  public
    procedure OnMove(Pos: TDblPoint; Shift: TShiftState); override;
  end;

  { TFreehandTool }

  TFreehandTool = class(TDrawingTool)
  public
    constructor Create(AIconName: string); override;
    procedure OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor); override;
    procedure OnMove(Pos: TDblPoint; Shift: TShiftState); override;
  private
    CurrentFigure: TFreehand;
  end;

  { TPolyLineTool }

  TPolyLineTool = class(TDrawingTool)
  public
    constructor Create(AIconName: string); override;
    procedure OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor); override;
    procedure OnMove(Pos: TDblPoint; Shift: TShiftState); override;
    procedure OnFinishedDrawing(Pos: TDblPoint); override;
    procedure OnOperationEnd; override;
  private
    CurrentFigure: TFreehand;
  end;

  { THandTool }

  THandTool = class(TTool)
  public
    procedure OnStartDrawing(Pos: TPoint); override;
    procedure OnMove(Pos: TPoint; Shift: TShiftState); override;
    procedure OnFinishedDrawing(Pos: TPoint); override;
  private
    LatestPoint: TPoint;
  end;

  { TSelectionTool }

  TSelectionTool = class(TDrawingTool)
  public
    constructor Create(AIconName: string); override;
    procedure Init(); override;
    procedure OnStartDrawing(Pos: TPoint); override;
    procedure OnMove(Pos: TPoint; Shift: TShiftState); override;
    procedure OnFinishedDrawing(Pos: TPoint); override; overload;
    procedure OnFinishedDrawing(Pos: TDblPoint); override; overload;
    procedure OnCanvasDblClick(Pos: TDblPoint); override;
    procedure OnToolChange(); override;
    procedure OnControlKeysStateChange(Shift: TShiftState); override;
    procedure DrawHelpers(ACanvas: TCanvas); override;
    destructor Destroy; override;
  private
    MultipleChoice, Moved: boolean;
    Start, Fin: TPoint;  // ?
    SelRgn: HRGN; // ??
    SelRect: TRect;

    procedure UpdateSelection();
    procedure BuildForSelected();
  end;

procedure RegisterTool(Tool: TTool);
function LoadIcon(ToolName: string): TBitmap;

var
  CurrentTool: TTool;
  Tools: array of TTool;
  IsDrawing: boolean;
  Editor: TFigureEditor;

implementation

procedure RegisterTool(Tool: TTool);
begin
  SetLength(Tools, Length(Tools) + 1);
  Tools[High(Tools)] := Tool;
end;

function LoadIcon(ToolName: string): TBitmap;
var
  Img: TPicture;
  Png: TPortableNetworkGraphic;
begin
  Img := TPicture.Create;
  try
    Png := TPortableNetworkGraphic.Create;
    Png.LoadFromFile('icons/' + ToolName + '.png');
    Img.Assign(Png);
  except
    Img := TPicture.Create;
    Img.Bitmap.SetSize(16, 16);
    Img.Bitmap.Canvas.Brush.Color := clActiveDark;
    Img.Bitmap.Canvas.Pen.Color := clNone;
    Img.Bitmap.Canvas.Rectangle(0, 0, 16, 16);
    Img.Bitmap.Canvas.Pen.Color := clRed;
    Img.Bitmap.Canvas.Pen.Width := 2;
    Img.Bitmap.Canvas.Line(0, 0, 15, 15);
    Img.Bitmap.Canvas.Line(15, 0, 0, 15);
  end;
  exit(Img.Bitmap);
end;

{ TDrawingTool }

procedure TDrawingTool.OnFinishedDrawing(Pos: TDblPoint);
begin
  inherited OnFinishedDrawing(Pos);
  UFigures.RememberScene();
end;

{ TSelectionTool }

constructor TSelectionTool.Create(AIconName: string);
begin
  inherited;
  Selected := TList.Create;
  MultipleChoice := False;
end;

procedure TSelectionTool.Init();
begin
  Editor.Cleanup();
end;

procedure TSelectionTool.OnStartDrawing(Pos: TPoint);
begin
  inherited OnStartDrawing(Pos);
  Active := True;
  Moved := False;
  Start := Pos;
  Fin := Pos;
end;

procedure TSelectionTool.OnMove(Pos: TPoint; Shift: TShiftState);
begin
  if Active then begin
    if not Moved then begin
      if (abs(Start.X - Pos.X) + abs(Start.Y - Pos.Y)) > 3 then Moved := True;
    end
    else begin
      Fin := Pos;
      UpdateSelection();
    end;
  end;
end;

procedure TSelectionTool.OnFinishedDrawing(Pos: TPoint);
var
  f: TFigure;
  x: integer;
  wasSelected: boolean;
begin
  if Moved then begin
    BuildForSelected();
  end
  else begin
    wasSelected := False;
    for f in Figures do begin
      if f.IsSelected(ScreenToWorld(Pos)) then begin
        if MultipleChoice then begin
          x := Selected.IndexOf(f);
          if x <> -1 then Selected.Delete(x)
                     else Selected.Add(f);
          wasSelected := True;
        end
        else begin
          Selected.Clear();
          Selected.Add(f);
          wasSelected := True;
        end;
      end;
    end;
    if not wasSelected then begin
      Selected.Clear();
    end;
    BuildForSelected();
  end;
end;

procedure TSelectionTool.OnFinishedDrawing(Pos: TDblPoint);
begin
  if Active then
     Active := False;
end;

procedure TSelectionTool.OnCanvasDblClick(Pos: TDblPoint);
begin
  inherited OnCanvasDblClick(Pos);
end;

procedure TSelectionTool.OnToolChange;
begin
  Selected.Clear;
  inherited OnToolChange;
end;

procedure TSelectionTool.OnControlKeysStateChange(Shift: TShiftState);
begin
  MultipleChoice := (ssCtrl in Shift);
end;

procedure TSelectionTool.DrawHelpers(ACanvas: TCanvas);
var
  f: TFigures;
  i: integer;
begin
  if (Active) then begin
    if (Moved) then begin
      ACanvas.Pen.Color := clBlue;
      ACanvas.Pen.Width := 1;
      ACanvas.Frame(SelRect);
      ACanvas.Pen.Color := clGray;
      ACanvas.Pen.Width := 1;
      ACanvas.Pen.Style := psDash;
      ACanvas.Frame(SelRect);
    end;
  end;
  if (Selected.Count > 0) then begin
    ACanvas.Pen.Color := clBlue;
    ACanvas.Pen.Style := psDash;
    ACanvas.Pen.Width := 1;

    SetLength(f, Selected.Count);
    for i := 0 to Selected.Count - 1 do begin
        f[i] := TFigure(Selected[i]);
        ACanvas.Frame(OutsetRect(
         WorldRectToScreenRect(f[i].GetClippingRectangle()), 2
        ));
    end;
    //ACanvas.Frame(OutsetRect(WorldRectToScreenRect(GetMultipleBounds(f)), 3));
  end;
end;

destructor TSelectionTool.Destroy;
begin
  FreeAndNil(Selected);
  inherited Destroy;
end;

procedure TSelectionTool.UpdateSelection;
var
  f: TFigure;
  b: boolean;
begin
  Selected.Clear;
  SelRect := NormalizeRect(Rect(Start.X, Start.Y, Fin.X, Fin.Y));
  if (SelRgn <> 0) then begin
    LCLIntf.DeleteObject(SelRgn);
  end;
  SelRgn := LCLIntf.CreateRectRgnIndirect(SelRect);
  for f in Figures do begin
    b := f.IsSelected(SelRgn);
    if b or f.IsSelected(ScreenRectToWorldRect(SelRect)) then
    begin
      Selected.Add(f);
    end;
  end;
end;

procedure TSelectionTool.BuildForSelected;
var
  a: TFigures;
  i: integer;
begin
  SetLength(a, Selected.Count);
  for i := 0 to Selected.Count - 1 do begin
    a[i] := TFigure(Selected[i]);
  end;
  Editor.Build(a);
end;

{ TTool }

constructor TTool.Create(AIconName: string);
begin
  Active := False;
  IconName := AIconName;
end;

procedure TTool.Init;
begin

end;

procedure TTool.OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor);
begin
  Active := True;
end;

procedure TTool.OnStartDrawing(Pos: TPoint);
begin

end;

procedure TTool.OnMove(Pos: TDblPoint; Shift: TShiftState);
begin

end;

procedure TTool.OnMove(Pos: TPoint; Shift: TShiftState);
begin

end;

procedure TTool.OnFinishedDrawing(Pos: TDblPoint);
begin
  Active := False;
end;

procedure TTool.OnFinishedDrawing(Pos: TPoint);
begin

end;

procedure TTool.OnCanvasClick(Pos: TDblPoint);
begin
  // Do nothing: it's not necessary for tool to implement this
end;

procedure TTool.OnCanvasDblClick(Pos: TDblPoint);
begin
  // Do nothing: it's not necessary for tool to implement this
end;

procedure TTool.OnToolChange;
begin
  Active := False;
end;

procedure TTool.OnControlKeysStateChange(Shift: TShiftState);
begin

end;

procedure TTool.OnOperationEnd;
begin

end;

procedure TTool.DrawHelpers(ACanvas: TCanvas);
begin

end;

function TTool.GetIcon(): TBitmap;
begin
  exit(UTools.LoadIcon(IconName));
end;

function TTool.IsActive: boolean;
begin
  exit(Active);
end;

{ TLinearTool }

constructor TLinearTool.Create(ALinFigureClass: TLinearFigureClass; AIconName: string);
begin
  inherited Create(AIconName);
  ResultingFigure := ALinFigureClass;
end;

procedure TLinearTool.Init;
begin
  inherited Init;
  Editor.Build(ResultingFigure);
end;

procedure TLinearTool.OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor);
begin
  CurrentFigure := ResultingFigure.Create(FGColor, BGColor);
  UFigures.AddFigure(CurrentFigure);

  CurrentFigure.First := DblPoint(Pos.x, Pos.y);
  CurrentFigure.Second := DblPoint(Pos.x, Pos.y);
  Center := Pos;

  Editor.ApplyTo(CurrentFigure);

  Active := True;
end;

procedure TLinearTool.OnMove(Pos: TDblPoint; Shift: TShiftState);
var
  deltax, deltay: extended;
  FirstPoint, SecondPoint: TDblPoint;
begin
  if Active then begin
    FirstPoint := CurrentFigure.First;
    SecondPoint := Pos;
    if ssShift in Shift then begin
      deltax := Pos.x - Center.x;
      deltay := Pos.y - Center.y;

      if ((deltax > 0) and (deltay > 0)) or ((deltax < 0) and (deltay < 0)) then begin
        SecondPoint.x := Center.x + Min(deltax, deltay);
        SecondPoint.y := Center.y + Min(deltax, deltay);
      end
      else if (deltax > 0) and (deltay < 0) then begin
        SecondPoint.x := Center.x - Min(deltax, deltay);
        SecondPoint.y := Center.y + Min(deltax, deltay);
      end
      else
      begin
        SecondPoint.x := Center.x + Min(deltax, deltay);
        SecondPoint.y := Center.y - Min(deltax, deltay);
      end;
    end;
    if ssCtrl in Shift then begin
      FirstPoint.x := Center.x - (SecondPoint.x - Center.x);
      FirstPoint.y := Center.y - (SecondPoint.y - Center.y);
    end
    else
      FirstPoint := DblPoint(Center.X, Center.Y);

    CurrentFigure.First := FirstPoint;
    CurrentFigure.Second := SecondPoint;
  end;
end;

{ TAngledLinearTool }

procedure TAngledLinearTool.OnMove(Pos: TDblPoint; Shift: TShiftState);
begin
  inherited OnMove(Pos, []); // Straight lines are harder than that
end;

constructor TFreehandTool.Create(AIconName: string);
begin
  inherited Create(AIconName);
end;

procedure TFreehandTool.OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor);
var
  SecondPos: TDblPoint;
begin
  Active := True;

  CurrentFigure := TFreehand.Create(FGColor, BGColor);
  UFigures.AddFigure(CurrentFigure);

  CurrentFigure.AddVertex(Pos);
  SecondPos.x := Pos.x + 1;
  SecondPos.y := Pos.y + 1;
  CurrentFigure.AddVertex(SecondPos);
end;

procedure TFreehandTool.OnMove(Pos: TDblPoint; Shift: TShiftState);
begin
  if Active then begin
    if Length(CurrentFigure.Vertices) = 2 then
      CurrentFigure.Vertices[1] := CurrentFigure.Vertices[0];
    CurrentFigure.AddVertex(Pos);
  end;
end;

{ TPolyLineTool }

constructor TPolyLineTool.Create(AIconName: string);
begin
  inherited Create(AIconName);
end;

procedure TPolyLineTool.OnStartDrawing(Pos: TDblPoint; FGColor, BGColor: TColor);
begin
  if not Active then begin
    CurrentFigure := TFreehand.Create(FGColor, BGColor);
    UFigures.AddFigure(CurrentFigure);
    CurrentFigure.AddVertex(Pos);
    Active := True;
  end;
  CurrentFigure.AddVertex(Pos);
end;

procedure TPolyLineTool.OnMove(Pos: TDblPoint; Shift: TShiftState);
begin
  if Active then
    CurrentFigure.vertices[High(CurrentFigure.vertices)] := Pos;
end;

procedure TPolyLineTool.OnFinishedDrawing(Pos: TDblPoint);
begin
  if Active then
    CurrentFigure.AddVertex(Pos);
end;

procedure TPolyLineTool.OnOperationEnd;
begin
  if Active then begin
    Active := False;
    UFigures.RememberScene();
  end;
end;

{ THandTool }

procedure THandTool.OnStartDrawing(Pos: TPoint);
begin
  inherited OnStartDrawing(Pos);
  LatestPoint := Pos;
end;

procedure THandTool.OnMove(Pos: TPoint; Shift: TShiftState);
begin
  if Active then begin
    inherited OnMove(Pos, Shift);
    UCoordinateTranslator.XOffset -= (LatestPoint.x - Pos.x);
    UCoordinateTranslator.YOffset -= (LatestPoint.y - Pos.y);
    LatestPoint := Pos;
  end;
end;

procedure THandTool.OnFinishedDrawing(Pos: TPoint);
begin
  inherited OnFinishedDrawing(Pos);
end;

initialization
  RegisterTool(TSelectionTool.Create('cursor'));
  RegisterTool(THandTool.Create('hand'));
  RegisterTool(TAngledLinearTool.Create(TLine, 'line'));
  RegisterTool(TPolyLineTool.Create('polyline'));
  RegisterTool(TLinearTool.Create(TRectangle, 'rectangle'));
  RegisterTool(TLinearTool.Create(TRoundRectangle, 'roundrectangle'));
  RegisterTool(TLinearTool.Create(TEllipse, 'ellipse'));
  RegisterTool(TFreehandTool.Create('freehand'));

end.
