unit UExUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, typinfo;

type
  TDblPoint = packed record
    X: double;
    Y: double;
  end;

  TDblRectEx = packed record
    TopLeft: TDblPoint;
    BottomRight: TDblPoint
  end;

function StrToPropVariant(str: string; info: PPropInfo): variant;

function DblPoint(AX, AY: double): TDblPoint;
function DblRect(X1, Y1, X2, Y2: double): TDblRectEx;
function DblRect(TopLeft, BottomRight: TDblPoint): TDblRectEx;

function NormalizeRect(ARect: TRect): TRect; overload;
function NormalizeRect(ADblRect: TDblRectEx): TDblRectEx; overload;

function HalfHeight(ARect: TRect): integer;
function HalfWidth(ARect: TRect): integer;

function OutsetRect(ARect: TDblRectEx; amt: double): TDblRectEx;
function OutsetRect(ARect: TRect; amt: integer): TRect;

function DoesIntersect(p0, p1, p2, p3: TDblPoint): boolean;
function DoesIntersect(x0, y0, x1, y1, x2, y2, x3, y3: double): boolean;

implementation

function StrToPropVariant(str: string; info: PPropInfo): variant;
begin
  case info^.PropType^.Kind of
    tkInteger:
    begin
      Result := StrToInt(str);
    end;
    tkFloat:
    begin
      Result := StrToFloat(str);
    end;
    tkString:
    begin
      Result := str;
    end;
    tkEnumeration:
    begin
      if (Length(str) > 0) and (Ord(str[1]) >= Ord('0')) and
        (Ord(str[1]) <= Ord('9')) then
        Result := StrToInt(str)
      else
        Result := GetEnumValue(info^.PropType, str);
    end
    else
    begin
      raise Exception.Create('Unsupported variant');
    end;
  end;
end;

function DblPoint(AX, AY: double): TDblPoint;
begin
  Result.X := AX;
  Result.Y := AY;
end;

function DblRect(X1, Y1, X2, Y2: double): TDblRectEx;
begin
  Result.TopLeft := DblPoint(Min(X1, X2), Min(Y1, Y2));
  Result.BottomRight := DblPoint(Max(X1, X2), Max(Y1, Y2));
end;

function DblRect(TopLeft, BottomRight: TDblPoint): TDblRectEx;
begin
  Result.TopLeft := DblPoint(Min(TopLeft.X, BottomRight.X),
                 Min(TopLeft.Y, BottomRight.Y));
  Result.BottomRight := DblPoint(Max(TopLeft.X, BottomRight.X),
                 Max(TopLeft.Y, BottomRight.Y));
end;

function NormalizeRect(ARect: TRect): TRect;
begin
  Result.Top := Min(ARect.Top, ARect.Bottom);
  Result.Left := Min(ARect.Left, ARect.Right);
  Result.Bottom := Max(ARect.Top, ARect.Bottom);
  Result.Right := Max(ARect.Left, ARect.Right);
end;

function NormalizeRect(ADblRect: TDblRectEx): TDblRectEx;
begin
  Result.TopLeft := DblPoint(
    Min(ADblRect.TopLeft.X, ADblRect.BottomRight.X),
    Min(ADblRect.TopLeft.Y, ADblRect.BottomRight.Y)
  );
  Result.BottomRight := DblPoint(
    Max(ADblRect.TopLeft.X, ADblRect.BottomRight.X),
    Max(ADblRect.TopLeft.Y, ADblRect.BottomRight.Y)
  );
end;

function HalfHeight(ARect: TRect): integer;
begin
  exit((ARect.Bottom - ARect.Top) div 2);
end;

function HalfWidth(ARect: TRect): integer;
begin
  exit((ARect.Right - ARect.Left) div 2);
end;

function OutsetRect(ARect: TDblRectEx; amt: double): TDblRectEx;
begin
  Result := ARect;
  Result.TopLeft.X -= amt;
  Result.TopLeft.Y -= amt;
  Result.BottomRight.X += amt;
  Result.BottomRight.Y += amt;
end;

function OutsetRect(ARect: TRect; amt: integer): TRect;
begin
  Result := ARect;
  Result.Top -= amt;
  Result.Left -= amt;
  Result.Right += amt;
  Result.Bottom += amt;
end;

function DoesIntersect(p0, p1, p2, p3: TDblPoint): boolean;
begin
  exit(
    DoesIntersect(
      p0.X, p0.Y, p1.X, p1.Y,
      p2.X, p2.Y, p3.X, p3.Y
    )
  )
end;

// LeMothe algorythm
function DoesIntersect(x0, y0, x1, y1, x2, y2, x3, y3: double): boolean;
var
  sx1, sy1, sx2, sy2, s, t, z: double;
begin
  sx1 := x1 - x0;
  sy1 := y1 - y0;
  sx2 := x3 - x2;
  sy2 := y3 - y2;

  z := (-sx2 * sy1 + sx1 * sy2);
  if z = 0 then exit(False);

  s := (-sy1 * (x0 - x2) + sx1 * (y0 - y2)) / z;
  t := ( sx2 * (y0 - y2) - sy2 * (x0 - x2)) / z;

  exit((s >= 0) and (s <= 1) and (t >= 0) and (t <= 1))
end;

end.

