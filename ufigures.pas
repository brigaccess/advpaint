unit UFigures;

{$mode objfpc}{$H+}{$M+}

interface

uses
  Classes, SysUtils, Graphics, UCoordinateTranslator, Math, LCLType, LCLIntf,
  UExUtils, typinfo, variants;

{TODO: Should we get rid of UCoordinateTranslator dependency here?}

type
  { Properties declaration }
  TPenWidth = type integer;
  TBorderRadius = type integer;

  { Figures declaration }

  { TFigure }

  TFigure = class(TPersistent)
  public
    constructor Create(FG, BG: TColor); virtual; overload;
    constructor Create(Description: string); virtual; overload;
    procedure Draw(Canvas: TCanvas); virtual;
    function GetClippingRectangle(): TDblRectEx; virtual; abstract;

    function IsSelected(ADblPoint: TDblPoint): Boolean; virtual;
    function IsSelected(ADblRect: TDblRectEx): Boolean; virtual;
    function IsSelected(ARgn: HRGN): Boolean; virtual;

    function ToStr(): string; virtual;
  private
    Pen: TPen;
    function GetFGColor: TColor;
    function GetPenStyle(): TPenStyle;
    function GetPenWidth(): integer;
    procedure SetFGColor(AValue: TColor);
    procedure SetPenStyle(AValue: TPenStyle);
    procedure SetPenWidth(AValue: TPenWidth);

    procedure UpdateSelectionInfo(); virtual;
  published
    property PenWidth: TPenWidth read GetPenWidth write SetPenWidth default 1;
    property PenStyle: TPenStyle read GetPenStyle write SetPenStyle default psSolid;
    property FGColor: TColor read GetFGColor write SetFGColor default clBlack;
  end;

  { TLinearFigure }

  TLinearFigure = class(TFigure)
  public
    constructor Create(Description: string); override;
    function GetClippingRectangle: TDblRectEx; override;

    function GetFirstPoint(): TDblPoint;
    procedure SetFirstPoint(ADblPoint: TDblPoint);
    function GetSecondPoint(): TDblPoint;
    procedure SetSecondPoint(ADblPoint: TDblPoint);

    function ToStr(): string; override;

    property First: TDblPoint read GetFirstPoint write SetFirstPoint;
    property Second: TDblPoint read GetSecondPoint write SetSecondPoint;
  private
    FirstPoint, SecondPoint: TDblPoint;
  end;

  // Helper definitions
  TFigures = array of TFigure;
  TFigureClass = class of TFigure;

  { TLine }

  TLine = class(TLinearFigure)
  public
    procedure Draw(Canvas: TCanvas); override;
    function IsSelected(ADblRect: TDblRectEx): Boolean; override;
    function IsSelected(ADblPoint: TDblPoint): Boolean; override;
  end;

  { TFilledLinearFigure }

  TFilledLinearFigure = class(TLinearFigure)
  public
    constructor Create(FG, BG: TColor); override; overload;
    procedure Draw(Canvas: TCanvas); override;
    function IsSelected(ADblPoint: TDblPoint): Boolean; override;
    function IsSelected(ARgn: HRGN): Boolean; override;
  private
    HandleRegion: HRGN;

    Brush: TBrush;
    function GetBGColor: TColor;
    function GetBrushStyle(): TBrushStyle;
    procedure SetBGColor(AValue: TColor);
    procedure SetBrushStyle(AValue: TBrushStyle);
    procedure UpdateSelectionInfo(); override;
  published
    property BrushStyle: TBrushStyle
      read GetBrushStyle write SetBrushStyle default bsSolid;
    property BGColor: TColor read GetBGColor write SetBGColor default clWhite;
  end;

  { TRectangle }
  TRectangle = class(TFilledLinearFigure)
  public
    procedure Draw(Canvas: TCanvas); override;
    function IsSelected(ARgn: HRGN): Boolean; override;
  private
    procedure UpdateSelectionInfo(); override;
  end;

  { TRoundRectangle }
  TRoundRectangle = class(TFilledLinearFigure)
  public
    constructor Create(FG, BG: TColor); override;
    procedure Draw(Canvas: TCanvas); override;
  private
    XRadius, YRadius: integer;
    function GetXBorderRadius(): TBorderRadius;
    function GetYBorderRadius(): TBorderRadius;
    procedure SetXBorderRadius(AValue: TBorderRadius);
    procedure SetYBorderRadius(AValue: TBorderRadius);
    procedure UpdateSelectionInfo(); override;
  published
    property XBorderRadius: TBorderRadius read GetXBorderRadius
      write SetXBorderRadius default 10;
    property YBorderRadius: TBorderRadius read GetYBorderRadius
      write SetYBorderRadius default 10;
  end;

  { TEllipse }

  TEllipse = class(TFilledLinearFigure)
  public
    procedure Draw(Canvas: TCanvas); override;
  private
    procedure UpdateSelectionInfo(); override;
  end;

  { TFreehand }

  TFreehand = class(TFigure)
  public
    Vertices: array of TDblPoint;

    constructor Create(Description: String); override;
    procedure Draw(Canvas: TCanvas); override;
    procedure AddVertex(Point: TDblPoint);

    function IsSelected(ADblRect: TDblRectEx): Boolean; override;
    function IsSelected(ADblPoint: TDblPoint): Boolean; override;

    function GetClippingRectangle: TDblRectEx; override;

    function ToStr(): String; override;
  private
    MinX, MinY, MaxX, MaxY: double;
  end;

var
  Figures: array of TFigure;
  DummyRgn: HRGN;
  History: TStringList;
  HistoryStep: integer = 0;
  LatestSave: integer = 0;
  Selected: TList;

procedure AddFigure(Figure: TFigure);
function GetLatest(): TFigure;
function GetWorldBounds(): TDblRectEx;
function GetMultipleBounds(AFigures: TFigures): TDblRectEx;
function GetUnderlyingFigures(ARect: TDblRectEx): TFigures;

procedure ClearScene();
procedure BuildScene(Scene: string); overload;
procedure BuildScene(Scene: array of string); overload;
procedure BuildFigure(Description: string);
function PresentScene(): String;

procedure Undo();
procedure Redo();
procedure RememberScene();
procedure ClearHistory();
function Changed(): boolean;

implementation

////////////////////////////////////////////////////////////////////////////////
//                           UTILITY FUNCTIONS                                //
////////////////////////////////////////////////////////////////////////////////

procedure AddFigure(Figure: TFigure);
begin
  SetLength(Figures, Length(Figures) + 1);
  Figures[High(Figures)] := Figure;
end;

procedure BuildScene(Scene: string);
var
  tmp: TStringList;
  s: string;
begin
  tmp := TStringList.Create;
  tmp.Text := Scene;
  for s in tmp do begin
    BuildFigure(s);
  end;
  FreeAndNil(tmp);
end;

procedure BuildScene(Scene: array of string);
var
  s: string;
begin
  ClearScene();
  for s in Scene do begin
    BuildFigure(s);
  end;
end;

procedure ClearScene;
begin
  SetLength(Figures, 0);
  Selected.Clear;
end;

procedure BuildFigure(Description: string);
var
  f: TFigure;
  ftype: string;
begin
  ftype := Copy(Description, 0, pos(':', Description) - 1);
  f := TFigureClass(FindClass(ftype)).Create(Description);
  AddFigure(f);
end;

function PresentScene: String;
var
  f: TFigure;
begin
  Result := '';
  for f in Figures do begin;
    Result += f.ToStr() + #13#10;
  end;
end;

procedure Undo;
begin
  if HistoryStep > 0 then begin
    HistoryStep -= 1;
    ClearScene();
    BuildScene(History.Strings[HistoryStep]);
  end;
end;

procedure Redo;
begin
  if HistoryStep + 1 < History.Count then begin
    HistoryStep += 1;
    ClearScene();
    BuildScene(History.Strings[HistoryStep]);
  end;
end;

procedure RememberScene;
var
  s: string;
begin
  s := PresentScene();
  if (HistoryStep > 0) and (s = History.Strings[HistoryStep]) then exit();
  // Removing "dead" history trees
  if HistoryStep < History.Count - 1 then begin
    while History.Count > HistoryStep + 1 do begin
      History.Delete(HistoryStep + 1);
    end;
    if HistoryStep < LatestSave then begin
        LatestSave := -1; // Cause we can't undo/redo to the initial state
    end;
  end;

  History.Add(s);
  HistoryStep += 1;
end;

procedure ClearHistory;
begin
  History.Clear;
  History.Add(PresentScene());
  HistoryStep := 0;
end;

function Changed: boolean;
begin
  exit(UFigures.LatestSave <> UFigures.HistoryStep);
end;

{TODO: Remove this function?}
function GetLatest(): TFigure;
begin
  exit(Figures[High(Figures)]);
end;

function GetWorldBounds: TDblRectEx;
begin
  exit(GetMultipleBounds(Figures));
end;

function GetMultipleBounds(AFigures: TFigures): TDblRectEx;
var
  i: integer;
  tmprect: TDblRectEx;
begin
  if Length(AFigures) = 0 then
  begin
    exit(DblRect(0, 0, 0, 0));
  end;

  Result := AFigures[0].GetClippingRectangle();
  if Length(AFigures) = 1 then
    exit();

  for i := 1 to High(Figures) do
  begin
    tmprect := AFigures[i].GetClippingRectangle();

    if tmprect.TopLeft.X < Result.TopLeft.X then
      Result.TopLeft.X := tmprect.TopLeft.X;
    if tmprect.TopLeft.Y < Result.TopLeft.Y then
      Result.TopLeft.Y := tmprect.TopLeft.Y;
    if tmprect.BottomRight.X > Result.BottomRight.X then
      Result.BottomRight.X := tmprect.BottomRight.X;
    if tmprect.BottomRight.Y > Result.BottomRight.Y then
      Result.BottomRight.Y := tmprect.BottomRight.Y;
  end;
end;

function GetUnderlyingFigures(ARect: TDblRectEx): TFigures;
var
  i: integer;
begin
  for i := 0 to High(Figures) do
  begin
    with Figures[i].GetClippingRectangle do
    begin
      if (TopLeft.X >= ARect.TopLeft.X) and
        (TopLeft.Y >= ARect.TopLeft.Y) and (BottomRight.X <=
        ARect.BottomRight.X) and (BottomRight.Y <= ARect.BottomRight.Y) then
      begin
        SetLength(Result, Length(Result) + 1);
        Result[High(Result)] := Figures[i];
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//                         FIGURE IMPLEMENTATION                              //
////////////////////////////////////////////////////////////////////////////////

{ TFigure }

constructor TFigure.Create(FG, BG: TColor);
begin
  Pen := TPen.Create;
  self.FGColor := FG;
end;

constructor TFigure.Create(Description: string);
var
  i, j, separator, appendix, topbound: integer;
  tmp: string;
  p: PPropList;
begin
  // Avoiding NPE
  self.Create(clBlack, clBlack);

  separator := pos(':', Description);
  appendix := pos('|', Description);
  if appendix <> 0 then topbound := appendix
                   else topbound := Length(Description);

  GetPropList(self, p);
  if separator <> 0 then
  begin
    tmp := '';
    j := 0;
    for i := separator + 1 to topbound do
    begin
      if Description[i] = ';' then
      begin
        SetPropValue(self, p^[j]^.Name, StrToPropVariant(tmp, p^[j]));
        tmp := '';
        inc(j);
        continue;
      end
      else
      begin
        tmp += Description[i];
      end;
    end;
  end
  else
  begin
    raise Exception.Create('Syntax error while deserializing figure: no class ' +
      'name separator found');
  end;
end;

procedure TFigure.Draw(Canvas: TCanvas);
begin
  Canvas.Pen := self.Pen;
end;

function TFigure.IsSelected(ADblPoint: TDblPoint): boolean;
begin
  exit(False);
end;

function TFigure.IsSelected(ADblRect: TDblRectEx): boolean;
begin
  exit(False);
end;

function TFigure.IsSelected(ARgn: HRGN): boolean;
begin
  exit(False);
end;

function TFigure.ToStr: string;
var
  p: PPropList;
  pt: PTypeInfo;
  i, size: integer;
begin
  Result := self.ClassName + ':';
  size := GetPropList(self, p);
  for i := 0 to size - 1 do
  begin
    pt := p^[i]^.PropType;
    Result += VarToStr(GetPropValue(self, p^[i]^.Name)) + ';';
  end;
end;

function TFigure.GetPenStyle: TPenStyle;
begin
  exit(self.Pen.Style);
end;

function TFigure.GetFGColor: TColor;
begin
  exit(self.Pen.Color);
end;

function TFigure.GetPenWidth: integer;
begin
  exit(self.Pen.Width);
end;

procedure TFigure.SetFGColor(AValue: TColor);
begin
  self.Pen.Color := AValue;
end;

procedure TFigure.SetPenStyle(AValue: TPenStyle);
begin
  self.Pen.Style := AValue;
end;

procedure TFigure.SetPenWidth(AValue: TPenWidth);
begin
  self.Pen.Width := AValue;
end;

procedure TFigure.UpdateSelectionInfo;
begin

end;

{ TLinearFigure }

constructor TLinearFigure.Create(Description: string);
var
  separator, i, j, lastpair, sep2: integer;
  working: string;
  pt: TDblPoint;
  x: string;
begin
  inherited Create(Description);
  separator := pos('|', Description);
  if separator <> 0 then begin
    j := 0;
    working := Copy(Description, separator + 1, Length(Description));
    lastpair := 0;
    for i := 0 to Length(working) do begin
      if working[i] = ';' then begin
        if j mod 2 = 0 then begin
          sep2 := i;
          j += 1;
          continue;
        end;
        pt := DblPoint(
          StrToFloat(Copy(working, lastpair, sep2 - lastpair - 1)),
          StrToFloat(Copy(working, sep2 + 1, i - sep2 - 1))
        );
        case j of
          1: self.First := pt;
          3: self.Second := pt;
          else begin
            raise Exception.Create('Too many coordinates');
          end;
        end;
        lastpair := i + 1;
        j += 1;
      end
    end;
  end
  else begin
    raise Exception.Create('No points specified for LinearFigure');
  end;
end;

function TLinearFigure.GetClippingRectangle: TDblRectEx;
begin
  exit(
    OutsetRect(
      DblRect(
        Min(First.X, Second.X), Min(First.Y, Second.Y),
        Max(First.X, Second.X), Max(First.Y, Second.Y)
      ), self.Pen.Width div 2
    )
  );
end;

function TLinearFigure.GetFirstPoint: TDblPoint;
begin
  exit(self.FirstPoint);
end;

procedure TLinearFigure.SetFirstPoint(ADblPoint: TDblPoint);
begin
  self.UpdateSelectionInfo();
  self.FirstPoint := ADblPoint;
end;

function TLinearFigure.GetSecondPoint: TDblPoint;
begin
  exit(self.SecondPoint);
end;

procedure TLinearFigure.SetSecondPoint(ADblPoint: TDblPoint);
begin
  self.UpdateSelectionInfo();
  self.SecondPoint := ADblPoint;
end;

function TLinearFigure.ToStr: string;
begin
  Result := inherited ToStr;
  Result += Format('|%f;%f;%f;%f;',
    [First.X, First.Y, Second.X, Second.Y]);
end;

{ TLine }

procedure TLine.Draw(Canvas: TCanvas);
begin
  inherited;
  Canvas.Line(WorldToScreen(First), WorldToScreen(Second));
end;

function TLine.IsSelected(ADblRect: TDblRectEx): Boolean;
begin
  with ADblRect do begin
    exit(
      // Upper bound
      DoesIntersect(First, Second, TopLeft, DblPoint(BottomRight.X, TopLeft.Y)) or
      // Right bound
      DoesIntersect(First, Second, TopLeft, DblPoint(TopLeft.X, BottomRight.Y)) or
      // Lower bound
      DoesIntersect(First, Second, BottomRight, DblPoint(TopLeft.X, BottomRight.Y)) or
      // Left bound
      DoesIntersect(First, Second, BottomRight, DblPoint(BottomRight.X, TopLeft.Y))
    )
  end;
end;

function TLine.IsSelected(ADblPoint: TDblPoint): Boolean;
var
  r: TDblRectEx;
begin
  r.TopLeft := ADblPoint;
  r.BottomRight := ADblPoint;
  r := OutsetRect(r, self.Pen.Width + (6 / UCoordinateTranslator.CurrentZoom));
  exit(self.IsSelected(r));
end;

{ TFilledLinearFigure }

constructor TFilledLinearFigure.Create(FG, BG: TColor);
begin
  inherited;
  Brush := TBrush.Create;
  self.BGColor := BG;
end;

procedure TFilledLinearFigure.Draw(Canvas: TCanvas);
begin
  inherited;
  Canvas.Brush := Brush;
  UpdateSelectionInfo();
end;

function TFilledLinearFigure.IsSelected(ADblPoint: TDblPoint): Boolean;
begin
  if (HandleRegion <> 0) then begin
    if (Brush.Style <> bsClear) then begin
      with WorldToScreen(ADblPoint) do
       exit(LCLIntf.PtInRegion(HandleRegion, X, Y))
    end
  end;
  self.UpdateSelectionInfo();
  exit(False)
end;

function TFilledLinearFigure.IsSelected(ARgn: HRGN): Boolean;
begin
  exit(NULLREGION <> CombineRgn(DummyRgn, HandleRegion, ARgn, RGN_AND));
end;

function TFilledLinearFigure.GetBrushStyle: TBrushStyle;
begin
  exit(self.Brush.Style);
end;

function TFilledLinearFigure.GetBGColor: TColor;
begin
  exit(self.Brush.Color);
end;

procedure TFilledLinearFigure.SetBGColor(AValue: TColor);
begin
  self.Brush.Color := AValue;
end;

procedure TFilledLinearFigure.SetBrushStyle(AValue: TBrushStyle);
begin
  if (self.Brush.Style <> AValue) then
    self.Brush.Style := AValue;
end;

procedure TFilledLinearFigure.UpdateSelectionInfo();
begin
  { TODO: Update existing region if possible }
  if (HandleRegion <> 0) then begin
    LCLIntf.DeleteObject(HandleRegion);
  end;
end;

{ TRectangle }

procedure TRectangle.Draw(Canvas: TCanvas);
begin
  inherited;
  Canvas.Rectangle(WorldToScreenRect(FirstPoint, SecondPoint));
end;

function TRectangle.IsSelected(ARgn: HRGN): Boolean;
var
  tmp: TRect;
begin
  LCLIntf.GetRgnBox(HandleRegion, @tmp);
  exit(LCLIntf.RectInRegion(ARgn, tmp));
end;

procedure TRectangle.UpdateSelectionInfo();
begin
  inherited;
  HandleRegion := LCLIntf.CreateRectRgnIndirect(
    NormalizeRect(WorldToScreenRect(FirstPoint, SecondPoint))
  );
end;

{ TRoundRectangle }

constructor TRoundRectangle.Create(FG, BG: TColor);
begin
  inherited;
  XRadius := 10; // TODO: Get it from property default value
  YRadius := 10; // -||-
end;

procedure TRoundRectangle.Draw(Canvas: TCanvas);
begin
  inherited;
  Canvas.RoundRect(WorldToScreenRect(FirstPoint, SecondPoint), XRadius, YRadius);
end;

function TRoundRectangle.GetXBorderRadius: TBorderRadius;
begin
  exit(self.XRadius);
end;

function TRoundRectangle.GetYBorderRadius(): TBorderRadius;
begin
  exit(self.YRadius);
end;

procedure TRoundRectangle.SetXBorderRadius(AValue: TBorderRadius);
begin
  self.XRadius := AValue;
end;

procedure TRoundRectangle.SetYBorderRadius(AValue: TBorderRadius);
begin
  self.YRadius := AValue;
end;

procedure TRoundRectangle.UpdateSelectionInfo();
begin
  inherited;
  with NormalizeRect(WorldToScreenRect(FirstPoint, SecondPoint)) do begin
    HandleRegion := LCLIntf.CreateRoundRectRgn(Left, Top, Right, Bottom,
      XRadius, YRadius);
  end;
end;

{ TEllipse }

procedure TEllipse.Draw(Canvas: TCanvas);
begin
  inherited;
  Canvas.Ellipse(WorldToScreenRect(First, Second));
end;

procedure TEllipse.UpdateSelectionInfo();
begin
  inherited;
  HandleRegion := LCLIntf.CreateEllipticRgnIndirect(
    NormalizeRect(WorldToScreenRect(FirstPoint, SecondPoint))
  );
end;

{ TFreehand }

constructor TFreehand.Create(Description: String);
var
  separator, i, j, latest: integer;
  working: string;
  x, z: string;
begin
  inherited Create(Description);
  separator := pos('|', Description);
  if separator <> 0 then begin
    latest := 0;
    j := 0;
    working := Copy(Description, separator + 1, Length(Description));
    for i := 0 to Length(working) do begin
      if working[i] = ';' then begin
        j += 1;
        if j = 2 then begin
          x := Copy(working, latest, separator - 1 - latest);
          z := Copy(working, separator + 1, i - separator - 1);
          self.AddVertex(DblPoint(
            StrToFloat(x),
            StrToFloat(z)
          ));
          latest := i + 1;
          j := 0;
          separator := i + 1;
        end
        else begin
          separator := i;
        end;
      end;
    end;
  end
  else begin
    raise Exception.Create('No points specified for Freehand');
  end;
end;

procedure TFreehand.Draw(Canvas: TCanvas);
var
  i: integer;
begin
  inherited;
  for i := 0 to High(Vertices) - 1 do
  begin
    Canvas.Line(WorldToScreen(Vertices[i]), WorldToScreen(Vertices[i + 1]));
  end;
end;

procedure TFreehand.AddVertex(Point: TDblPoint);
begin
  SetLength(Vertices, Length(Vertices) + 1);
  Vertices[High(Vertices)] := Point;

  if Length(Vertices) = 1 then begin
    MinX := Point.X;
    MaxX := Point.X;
    MinY := Point.Y;
    MaxY := Point.Y;
  end
  else begin
   if Point.X < MinX then
     MinX := Point.X;
   if Point.X > MaxX then
     MaxX := Point.X;
   if Point.Y < MinY then
     MinY := Point.Y;
   if Point.Y > MaxY then
     MaxY := Point.Y;
  end;
end;

function TFreehand.IsSelected(ADblRect: TDblRectEx): Boolean;
var
  i: integer;
begin
  Result := False;
  with ADblRect do begin
     for i := 0 to High(Vertices) - 1 do begin
       Result := Result or (
          // Upper bound
          DoesIntersect(Vertices[i], Vertices[i + 1], TopLeft,
        DblPoint(BottomRight.X, TopLeft.Y))
          // Right bound
       or DoesIntersect(Vertices[i], Vertices[i + 1], TopLeft,
        DblPoint(TopLeft.X, BottomRight.Y))
          // Lower bound
       or DoesIntersect(Vertices[i], Vertices[i + 1], BottomRight,
        DblPoint(TopLeft.X, BottomRight.Y))
          // Left bound
       or DoesIntersect(Vertices[i], Vertices[i + 1], BottomRight,
        DblPoint(BottomRight.X, TopLeft.Y))
//       or PtInRgn
      );
     end;
  end;
end;

function TFreehand.IsSelected(ADblPoint: TDblPoint): Boolean;
var
  r: TDblRectEx;
begin
  r.TopLeft := ADblPoint;
  r.BottomRight := ADblPoint;
  r := OutsetRect(r, self.Pen.Width + (6 / UCoordinateTranslator.CurrentZoom));
  exit(self.IsSelected(r));
end;

function TFreehand.GetClippingRectangle: TDblRectEx;
begin
  exit(DblRect(MinX, MinY, MaxX, MaxY));
end;

function TFreehand.ToStr: String;
var
  p: TDblPoint;
begin
  Result := inherited ToStr + '|';
  for p in self.Vertices do begin
    Result += Format('%f;%f;', [p.X, p.Y]);
  end;
end;


initialization
  DummyRgn := CreateRectRgn(0, 0, 0, 0);
  RegisterClass(TLine);
  RegisterClass(TRectangle);
  RegisterClass(TRoundRectangle);
  RegisterClass(TEllipse);
  RegisterClass(TFreehand);

  History := TStringList.Create;
  History.Sorted := False;
  History.Add('');
end.
