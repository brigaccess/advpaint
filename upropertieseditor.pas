unit UPropertiesEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Graphics, UFigures, typinfo, LCLType, StdCtrls,
  Spin, FPCanvas;

type

  TAbstractFigureEditor = class(TObject)
  public
    Panel: TWinControl;

    procedure OnChange(); virtual; abstract;
  end;

  { TPropertyEditor }

  TPropertyEditor = class(TObject)
  public
    constructor Create(AParent: TAbstractFigureEditor; APropInfo: PPropInfo);
      virtual;

    procedure ApplyTo(AFigure: TFigure); virtual;
    procedure UpdateWith(AFigure: TFigure); virtual;

    function Render(): TControl; virtual; abstract;

    function GetPropName(): string;

    function GetValue(): Variant; virtual; abstract;
    procedure SetValue(NewValue: Variant); virtual; abstract;
  private
    FPropInfo: PPropInfo;
    FParent: TAbstractFigureEditor;

    ShouldUpdate: Boolean;

    procedure HandleChange(ASender: TObject); virtual;
  end;

  TEditorClass = class of TPropertyEditor;

  { TFigureEditor }

  TFigureEditor = class(TAbstractFigureEditor)
  public
    constructor Create(AToolsPanel: TWinControl; ACanvasCtl: TControl);

    procedure Build(AFigure: TFigure);
    procedure Build(AFigures: TFigures);
    procedure Build(AFigureClass: TClass);

    procedure ApplyTo(AFigure: TFigure);
    procedure ApplyTo(AFigures: TFigures);

    procedure ForceChange(APropName: string; AValue: Variant);

    procedure OnChange(); override;
    procedure Cleanup();

    destructor Destroy; override;
  private
    AssignedFigures: array of TFigure;
    BuiltEditors: array of TPropertyEditor;

    Invalidation: procedure of object;

    procedure AppendEditor(AEditor: TPropertyEditor);

    procedure ApplyTo(AFigure: TFigure; Redraw: boolean);

    procedure Assign(AFigure: TFigure);
    procedure Assign(AFigures: TFigures);

    procedure Build(AFigure: TFigure; Clean: Boolean);
    procedure Build(AFigureClass: TClass; Clean: Boolean);

    procedure ClearPanel();

    function AlreadyHasEditor(APropName: string): Boolean;

    procedure Update();

    procedure Render();
  end;

  ////////////////////////////////////////////////////////////////////////////
  //                           Default editors                              //
  ////////////////////////////////////////////////////////////////////////////

  { TNumericalPropertyEditor }

  TNumericalPropertyEditor = class(TPropertyEditor)
  public
    function Render(): TControl; override;

    function GetValue(): Variant; override;
    procedure SetValue(NewValue: Variant); override;
  private
    SpinEdit: TSpinEdit;
  end;

  { TPenStyleEditor }

  TPenStyleEditor = class(TPropertyEditor)
  public
    function Render(): TControl; override;

    function GetValue(): Variant; override;
    procedure SetValue(NewValue: Variant); override;
  private
    ComboBox: TComboBox;

    procedure DrawItem(Control: TWinControl; Index: integer;
      ARect: TRect; State: TOwnerDrawState);
  end;

  { TBrushStyleEditor }

  TBrushStyleEditor = class(TPropertyEditor)
  public
    function Render(): TControl; override;

    function GetValue(): Variant; override;
    procedure SetValue(NewValue: Variant); override;
  private
    ComboBox: TComboBox;
    procedure DrawItem(Control: TWinControl; Index: integer;
      ARect: TRect; State: TOwnerDrawState);
  end;

  ////////////////////////////////////////////////////////////////////////////
  //                               Registry                                 //
  ////////////////////////////////////////////////////////////////////////////

  TEditorRegistryItem = record
    TypeName: string;
    Assigned: TEditorClass;
  end;

  TEditorDescrItem = record
    PropName: string;
    Description: string;
  end;

  procedure RegisterEditorFor(AEditor: TClass; ATypeName: String);
  { TODO: This should be in localization }
  procedure RegisterAlias(APropName: String; ADescription: String);

  var
    EditorsRegistry: array of TEditorRegistryItem;
    EditorsDescr: array of TEditorDescrItem;

implementation


procedure RegisterEditorFor(AEditor: TClass; ATypeName: String);
begin
  SetLength(EditorsRegistry, Length(EditorsRegistry) + 1);
  with EditorsRegistry[High(EditorsRegistry)] do begin
    TypeName := ATypeName;
    try
       Assigned := TEditorClass(AEditor);
    except
       { TODO: Console log? }
    end;
  end;
end;

procedure RegisterAlias(APropName: String; ADescription: String);
begin
  SetLength(EditorsDescr, Length(EditorsDescr) + 1);
  with EditorsDescr[High(EditorsDescr)] do begin
    PropName := APropName;
    Description := ADescription;
  end;
end;

{ TPropertyEditor }

constructor TPropertyEditor.Create(AParent: TAbstractFigureEditor;
  APropInfo: PPropInfo);
begin
  FPropInfo := APropInfo;
  FParent := AParent;
  ShouldUpdate := True;
end;

procedure TPropertyEditor.ApplyTo(AFigure: TFigure);
begin
  if (AFigure <> Nil) then begin
    try
       if (GetPropInfo(AFigure, FPropInfo^.Name) <> Nil) then begin
         SetPropValue(AFigure, FPropInfo^.Name, self.GetValue());
       end;
    finally
    end;
  end;
end;

procedure TPropertyEditor.UpdateWith(AFigure: TFigure);
begin
  if (AFigure <> Nil) then begin
    try
      if (GetPropInfo(AFigure, FPropInfo^.Name) <> Nil) then begin
        ShouldUpdate := False;
        self.SetValue(GetPropValue(AFigure, FPropInfo^.Name));
        ShouldUpdate := True;
      end;
    finally
    end;
  end;
end;

function TPropertyEditor.GetPropName: string;
begin
  exit(FPropInfo^.Name);
end;

procedure TPropertyEditor.HandleChange(ASender: TObject);
begin
  if ShouldUpdate then FParent.OnChange();
end;

{ TFigureEditor }

constructor TFigureEditor.Create(AToolsPanel: TWinControl; ACanvasCtl: TControl);
begin
  Panel := AToolsPanel;
  Invalidation := @ACanvasCtl.Invalidate;
end;

procedure TFigureEditor.Build(AFigure: TFigure);
begin
  self.Build(AFigure, True);
end;

procedure TFigureEditor.Build(AFigure: TFigure; Clean: Boolean);
begin
  if Clean then Cleanup();

  Build(AFigure.ClassType, False);
  Assign(AFigure);
end;

procedure TFigureEditor.Build(AFigures: TFigures);
var
  x: TFigure;
begin
  Cleanup();

  for x in AFigures do begin
    Build(x, False);
  end;

  Assign(AFigures);
end;

procedure TFigureEditor.Build(AFigureClass: TClass);
begin
  self.Build(AFigureClass, True);
end;

procedure TFigureEditor.Build(AFigureClass: TClass; Clean: Boolean);
var
  p: PPropList;
  size, i: Integer;
  x: TEditorRegistryItem;
begin
  if Clean then Cleanup();

  size := GetPropList(AFigureClass, p);
  for i := 0 to size - 1 do begin
    for x in EditorsRegistry do begin
      if (not AlreadyHasEditor(p^[i]^.Name)) and
         (p^[i]^.PropType^.Name = x.TypeName) then
      begin
        AppendEditor(x.Assigned.Create(self, p^[i]));
      end;
    end;
  end;

  Render();
end;

procedure TFigureEditor.ClearPanel;
var
  i: integer;
begin
  if Panel <> Nil then begin
    for i := Panel.ControlCount - 1 downto 0 do begin
      Panel.Controls[i].Free;
    end;
  end;
end;

function TFigureEditor.AlreadyHasEditor(APropName: string): Boolean;
var
  p: TPropertyEditor;
begin
  for p in BuiltEditors do begin
    if p.GetPropName() = APropName then exit(True);
  end;
  exit(False);
end;

procedure TFigureEditor.ApplyTo(AFigure: TFigure);
begin
  ApplyTo(AFigure, True);
  Assign(AFigure);
end;

procedure TFigureEditor.ApplyTo(AFigure: TFigure; Redraw: boolean);
var
  x: TPropertyEditor;
begin
  if (AFigure <> Nil) then begin
    for x in BuiltEditors do begin
      x.ApplyTo(AFigure);
    end;
  end;

  if (Redraw) then Invalidation();
end;

procedure TFigureEditor.ApplyTo(AFigures: TFigures);
var
  x: TFigure;
begin
  for x in AFigures do begin
    ApplyTo(x, False);
  end;
  Assign(AFigures);
  Invalidation();
end;

procedure TFigureEditor.ForceChange(APropName: string; AValue: Variant);
var
  x: TPropertyEditor;
  f: TFigure;
  changed: boolean;
begin
  for x in BuiltEditors do begin
    if x.GetPropName() = APropName then begin
      x.SetValue(AValue);
      OnChange();
      exit();
    end;
  end;

  // Assuming that we don't have the editor for property
  // But we still have to process the request
  changed := False;
  for f in AssignedFigures do begin
    if (GetPropInfo(f, APropName) <> Nil) then begin
      SetPropValue(f, APropName, AValue);
      changed := True;
    end;
  end;

  if (changed) then OnChange();
end;

procedure TFigureEditor.OnChange;
begin
  ApplyTo(AssignedFigures);
  UFigures.RememberScene();
end;

procedure TFigureEditor.Cleanup;
begin
  SetLength(BuiltEditors, 0);
  SetLength(AssignedFigures, 0);

  ClearPanel();
end;

destructor TFigureEditor.Destroy;
var
  x: TPropertyEditor;
begin
  Cleanup();
  for x in BuiltEditors do begin
    FreeAndNil(x);
  end;
  inherited Destroy;
end;

procedure TFigureEditor.AppendEditor(AEditor: TPropertyEditor);
begin
  SetLength(BuiltEditors, Length(BuiltEditors) + 1);
  BuiltEditors[High(BuiltEditors)] := AEditor;
end;

procedure TFigureEditor.Assign(AFigure: TFigure);
begin
  SetLength(AssignedFigures, 1);
  AssignedFigures[0] := AFigure;
  Update();
end;

procedure TFigureEditor.Assign(AFigures: TFigures);
begin
  AssignedFigures := AFigures;
  Update();
end;

procedure TFigureEditor.Update;
var
  e: TPropertyEditor;
  f: TFigure;
begin
  for e in BuiltEditors do begin
    for f in AssignedFigures do e.UpdateWith(f);
  end;
end;

procedure TFigureEditor.Render;
var
  e: TPropertyEditor;

  d: TEditorDescrItem;
  tempdesc: string;

  l: TLabel;
  rendered: TControl;
begin
  ClearPanel();
  for e in BuiltEditors do begin
    rendered := e.Render();

    if (rendered <> Nil) then begin
      tempdesc := e.GetPropName();

      for d in EditorsDescr do begin
        if d.PropName = tempdesc then tempdesc := d.Description;
      end;

      l := TLabel.Create(Panel);
      l.Layout := tlCenter;
      l.Caption := tempdesc;
      l.Parent := Panel;

      rendered.Parent := Panel;
    end;
  end;
end;

{ TNumericalPropertyEditor }

function TNumericalPropertyEditor.Render(): TControl;
begin
  Result := TSpinEdit.Create(FParent.Panel);
  with (Result as TSpinEdit) do
  begin
    Width := 50;
    Height := 20;

    ParentFont := False;
    Font.Height := 12;

    MinValue := 0;
    MaxValue := 100;
    Value := GetValue();

    OnChange := @self.HandleChange;
  end;
  SpinEdit := (Result as TSpinEdit);
end;

function TNumericalPropertyEditor.GetValue: Variant;
begin
  if (SpinEdit <> Nil) then exit(SpinEdit.Value)
  else exit(FPropInfo^.Default);
end;

procedure TNumericalPropertyEditor.SetValue(NewValue: Variant);
begin
  if (SpinEdit <> Nil) then begin
    SpinEdit.Value := NewValue;
  end;
end;

{ TPenStyleEditor }

function TPenStyleEditor.Render: TControl;
var
  i: integer;
begin
  ComboBox := TComboBox.Create(FParent.Panel);
  with ComboBox do
  begin
    Constraints.MaxHeight := 20;
    Constraints.MaxWidth := 53;

    ParentFont := False;
    Font.Height := 12;

    Style := csOwnerDrawVariable;
    ReadOnly := True;

    for i := 0 to Ord(High(TFPPenStyle)) do
    begin
      Items.Add('');
    end;
    ItemIndex := Ord(psSolid);

    TabStop := False;

    OnDrawItem := @self.DrawItem;
    OnChange := @self.HandleChange;
  end;
  Result := ComboBox;
end;

function TPenStyleEditor.GetValue: Variant;
begin
  if (ComboBox <> Nil) then exit(TFPPenStyle(ComboBox.ItemIndex))
  else exit(FPropInfo^.Default);
end;

procedure TPenStyleEditor.SetValue(NewValue: Variant);
begin
  if (ComboBox <> Nil) then begin
    ComboBox.ItemIndex := GetEnumValue(TypeInfo(TFPPenStyle), NewValue);
  end;
end;

procedure TPenStyleEditor.DrawItem(Control: TWinControl; Index: integer;
  ARect: TRect; State: TOwnerDrawState);
begin
  with ComboBox.Canvas do
  begin
    // Resetting
    Pen.Width := 1;
    Pen.EndCap := pecSquare;

    // Choosing background based on cell state
    Brush.Color := clWindow;
    if (odSelected in State) and not (odComboBoxEdit in State) then
    begin
      Brush.Color := clHighlight;
    end;
    // Drawing cell bg
    Pen.Color := Brush.Color;
    Pen.Style := psSolid;
    Rectangle(ARect);
    // Drawing bounding rectangle
    Pen.Color := clDefault;
    Rectangle(ARect.Left + 2, ARect.Top + 2, ARect.Right - 2, ARect.Bottom - 2);
    // Drawing example line
    if Index <> (Control as TComboBox).Items.Count - 1 then
    begin
      Pen.Width := 2;
      Pen.Style := TFPPenStyle(Index);
      Line(ARect.Left + 4, ARect.Top + (ARect.Bottom - ARect.Top) div 2,
        ARect.Right - 4, ARect.Top + (ARect.Bottom - ARect.Top) div 2);
    end;
  end;
end;

{ TBrushStyleEditor }

function TBrushStyleEditor.Render: TControl;
var
  i: integer;
begin
  ComboBox := TComboBox.Create(FParent.Panel);
  with ComboBox do
  begin
    Constraints.MaxHeight := 20;
    Constraints.MaxWidth := 53;

    ParentFont := False;
    Font.Height := 12;

    Style := csOwnerDrawVariable;
    ReadOnly := True;

    for i := 0 to Ord(High(TFPBrushStyle)) - 2 do
    begin
      Items.Add('');
    end;
    ItemIndex := Ord(bsSolid);

    TabStop := False;

    OnDrawItem := @self.DrawItem;
    OnChange := @self.HandleChange;
  end;
  Result := ComboBox;
end;

procedure TBrushStyleEditor.DrawItem(Control: TWinControl; Index: integer;
  ARect: TRect; State: TOwnerDrawState);
begin
  with ComboBox.Canvas do begin
    // Reset canvas state
    Pen.Width := 1;
    Pen.EndCap := pecSquare;

    // Choose background based on cell state
    Brush.Color := clWindow;
    if (odSelected in State) and not (odComboBoxEdit in State) then
    begin
      Brush.Color := clHighlight;
    end;
    // Draw cell bg
    Pen.Color := Brush.Color;
    Pen.Style := psSolid;
    Rectangle(ARect);
    // Drawing rectangle
    Pen.Color := clDefault;
    Brush.Color := clBackground;
    Brush.Style := TFPBrushStyle(Index);
    Rectangle(ARect.Left + 2, ARect.Top + 2, ARect.Right - 2, ARect.Bottom - 2);
  end;
end;

function TBrushStyleEditor.GetValue: Variant;
begin
  if (ComboBox <> Nil) then exit(TBrushStyle(ComboBox.ItemIndex))
  else exit(FPropInfo^.Default);
end;

procedure TBrushStyleEditor.SetValue(NewValue: Variant);
begin
  if (ComboBox <> Nil) then begin
    ComboBox.ItemIndex := GetEnumValue(TypeInfo(TFPBrushStyle), NewValue);
  end;
end;

initialization
RegisterEditorFor(TNumericalPropertyEditor.ClassType, 'integer');
RegisterEditorFor(TNumericalPropertyEditor.ClassType, 'TPenWidth');
RegisterEditorFor(TPenStyleEditor.ClassType, 'TFPPenStyle');
RegisterEditorFor(TBrushStyleEditor.ClassType, 'TFPBrushStyle');
RegisterEditorFor(TNumericalPropertyEditor.ClassType, 'TBorderRadius');

RegisterAlias('PenWidth', 'Pen width:');
RegisterAlias('PenStyle', 'Pen style:');
RegisterAlias('BrushStyle', 'Brush style:');
RegisterAlias('XBorderRadius', 'X Radius:');
RegisterAlias('YBorderRadius', 'Y Radius:');
//RegisterAlias('', 'Brush style:');

end.

