unit USVGFormat;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, UFigures, UFormats, UExUtils, XMLWrite, DOM, Math;

type

  { TSVGExporter }

  TSVGExporter = class(TExporter)
  public
    procedure Save(Filename: string; Figures: TFigures; Canvas: TCanvas); override;
    // TODO Register custom figures' way to render in SVG
  end;

implementation

{ TSVGExporter }

procedure TSVGExporter.Save(Filename: string; Figures: TFigures; Canvas: TCanvas
  );
var
  Doc: TXMLDocument;
  Root: TDOMNode;
  fe: TDOMElement;
  f: TFigure;
  viewBoxDbl, clip: TDblRectEx;
  v: TDblPoint;
  tmps: string;
  width, height: integer;
begin
  try
  Doc := TXMLDocument.Create;
  Doc.XMLVersion := '1.0';
  Doc.XMLStandalone := False;

  Root := Doc.CreateElement('svg');
  TDOMElement(Root).SetNSI('http://www.w3.org/2000/svg', 0);
  viewBoxDbl := NormalizeRect(UFigures.GetMultipleBounds(Figures));
  with viewBoxDbl do begin
  width := Math.ceil(BottomRight.X - TopLeft.X) + 1;
  height := Math.ceil(BottomRight.Y - TopLeft.Y) + 1;
  TDOMElement(Root).SetAttribute('viewBox', Format('%d %d %d %d',
  [
    Math.floor(TopLeft.X), Math.floor(TopLeft.Y),
    Math.ceil(BottomRight.X) - Math.floor(TopLeft.X) + 1,
    Math.ceil(BottomRight.Y) - Math.floor(TopLeft.Y) + 1
   ])
  );
  end;

  TDOMElement(Root).SetAttribute('width', IntToStr(width));
  TDOMElement(Root).SetAttribute('height', IntToStr(height));
  TDOMElement(Root).SetAttribute('version', '1.1');
  Doc.AppendChild(Root);
  for f in Figures do begin
    case f.ClassName of
    'TRectangle', 'TRoundRectangle': begin
      fe := Doc.CreateElement('rect');
      clip := NormalizeRect(
        OutsetRect(f.GetClippingRectangle(), -f.PenWidth div 2)
      );
      with clip do begin
        fe.SetAttribute('x', IntToStr(Math.Floor(TopLeft.X)));
        fe.SetAttribute('y', IntToStr(Math.Floor(TopLeft.Y)));
        fe.SetAttribute('width',
          IntToStr(Math.Floor(BottomRight.X - TopLeft.X)));
        fe.SetAttribute('height',
          IntToStr(Math.Floor(BottomRight.Y - TopLeft.Y)));
      end;
      if f.ClassName = 'TRoundRectangle' then begin
        fe.SetAttribute('rx', IntToStr(TRoundRectangle(f).XBorderRadius));
        fe.SetAttribute('ry', IntToStr(TRoundRectangle(f).YBorderRadius));
      end;
    end;
    'TLine': begin
      fe := Doc.CreateElement('line');
      fe.SetAttribute('x1', IntToStr(round(TLine(f).First.X)));
      fe.SetAttribute('x2', IntToStr(round(TLine(f).Second.X)));
      fe.SetAttribute('y1', IntToStr(round(TLine(f).First.Y)));
      fe.SetAttribute('y2', IntToStr(round(TLine(f).Second.Y)));
    end;
    'TFreehand': begin
      tmps := '';
      for v in TFreehand(f).Vertices do begin
        if tmps <> '' then tmps += ' ';
        tmps += Format('%d,%d', [round(v.X), round(v.Y)]);
      end;
      fe := Doc.CreateElement('polyline');
      fe.SetAttribute('points', tmps);
      fe.SetAttribute('fill', 'none');
    end;
    'TEllipse': begin
      fe := Doc.CreateElement('ellipse');
      clip := NormalizeRect(
        OutsetRect(f.GetClippingRectangle(), -f.PenWidth div 2)
      );
      with clip do begin
        fe.SetAttribute('cx', IntToStr(Math.Floor(TopLeft.X) + Math.Floor((BottomRight.X - TopLeft.X) / 2)));
        fe.SetAttribute('cy', IntToStr(Math.Floor(TopLeft.Y) + Math.Floor((BottomRight.Y - TopLeft.Y) / 2)));
        fe.SetAttribute('rx', IntToStr(Math.Floor((BottomRight.X - TopLeft.X) / 2) - f.PenWidth div 2));
        fe.SetAttribute('ry', IntToStr(Math.Floor((BottomRight.Y - TopLeft.Y) / 2) - f.PenWidth div 2));
      end;
    end
    else begin

    end;
    end;
    if f is TFilledLinearFigure then begin
      fe.SetAttribute('fill', '#' + IntToHex(ColorToRGB(TRectangle(f).BGColor), 6));
    end;
    fe.SetAttribute('stroke', '#' + IntToHex(ColorToRGB(f.FGColor), 6));
    fe.SetAttribute('stroke-width', IntToStr(f.PenWidth));
    Root.AppendChild(fe)
  end;

  WriteXMLFile(Doc, Filename);
  finally
    FreeAndNil(Doc);
  end;
end;

initialization
UFormats.RegisterExporter('SVG 1.1|*.svg', TSVGExporter.Create());

end.

