unit UCoordinateTranslator;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, UExUtils;

function WorldToScreen(ADblPoint: TDblPoint): TPoint;
function ScreenToWorld(APoint: TPoint): TDblPoint;

function WorldToScreenRect(ADblPoint1, ADblPoint2: TDblPoint): TRect;
function WorldRectToScreenRect(ADblRectEx: TDblRectEx): TRect;
function ScreenRectToWorldRect(ARect: TRect): TDblRectEx;

procedure SetZoom(AMultiplier: double);
procedure ZoomIn(APoint: TPoint; AViewport: TRect);
procedure ZoomOut(APoint: TPoint; AViewport: TRect);
procedure SetViewport(Left: double; Top: double; Right: double;
  Bottom: double; CurrentViewport: TRect);
procedure CenterOn(ADblPoint: TDblPoint; Viewport: TRect);
procedure FocusOn(FocusPoint: TDblPoint; ScreenPoint: TPoint; Viewport: TRect);

const
  ZOOM_MULT = sqrt(2);
  ZOOM_MIN = 0.01;
  ZOOM_MAX = 80;
  ZOOM_FIT_OFFSET = 0.02;

var
  CurrentZoom: double = 1;
  XOffset, YOffset: double;

implementation



function WorldToScreen(ADblPoint: TDblPoint): TPoint;
begin
  Result.X := Round((ADblPoint.X * CurrentZoom) + XOffset);
  Result.Y := Round((ADblPoint.Y * CurrentZoom) + YOffset);
end;

function ScreenToWorld(APoint: TPoint): TDblPoint;
begin
  Result.X := (APoint.X - XOffset) / CurrentZoom;
  Result.Y := (APoint.Y - YOffset) / CurrentZoom;
end;

function WorldToScreenRect(ADblPoint1, ADblPoint2: TDblPoint): TRect;
begin
  Result.TopLeft := WorldToScreen(ADblPoint1);
  Result.BottomRight := WorldToScreen(ADblPoint2);
end;

function WorldRectToScreenRect(ADblRectEx: TDblRectEx): TRect;
var
  tmpDblPoint: TPoint;
begin
  Result.TopLeft := WorldToScreen(ADblRectEx.TopLeft);
  Result.BottomRight := WorldToScreen(ADblRectEx.BottomRight);
end;

function ScreenRectToWorldRect(ARect: TRect): TDblRectEx;
begin
  Result.TopLeft := ScreenToWorld(ARect.TopLeft);
  Result.BottomRight := ScreenToWorld(ARect.BottomRight);
  Result := NormalizeRect(Result);
end;

procedure SetZoom(AMultiplier: double);
var
  newmult: double;
begin
  newmult := AMultiplier;
  if newmult < ZOOM_MIN then
  begin
    newmult := ZOOM_MIN;
    exit();
  end;
  if newmult > ZOOM_MAX then
  begin
    newmult := ZOOM_MAX;
    exit();
  end;
  if newmult > CurrentZoom then
  begin
    XOffset *= newmult;
    YOffset *= newmult;
  end
  else if newmult > CurrentZoom then
  begin
    XOffset /= newmult;
    YOffset /= newmult;
  end;
  CurrentZoom := newmult;
end;

procedure ZoomIn(APoint: TPoint; AViewport: TRect);
var
  worldPoint: TDblPoint;
begin
  worldPoint := ScreenToWorld(APoint);
  SetZoom(CurrentZoom * ZOOM_MULT);
  FocusOn(worldPoint, APoint, AViewport);
end;

procedure ZoomOut(APoint: TPoint; AViewport: TRect);
var
  worldPoint: TDblPoint;
begin
  worldPoint := ScreenToWorld(APoint);
  SetZoom(CurrentZoom / ZOOM_MULT);
  FocusOn(worldPoint, APoint, AViewport);
end;

procedure SetViewport(Left: double; Top: double; Right: double;
  Bottom: double; CurrentViewport: TRect);
var
  dx, dy: double;
begin
  dx := Max(Left, Right) - Min(Left, Right);
  dy := Max(Top, Bottom) - Min(Top, Bottom);

  if (dx > 0) and (dy > 0) then
  begin
    SetZoom(
      Min((CurrentViewport.Right - 1 - CurrentViewport.Right * ZOOM_FIT_OFFSET) /
      dx, (CurrentViewport.Bottom - 1 - CurrentViewport.Bottom *
      ZOOM_FIT_OFFSET) / dy)
      );
  end;
end;

procedure CenterOn(ADblPoint: TDblPoint; Viewport: TRect);
var
  mid: TPoint;
begin
  mid := Point(Round(Viewport.Right / 2), Round(Viewport.Bottom / 2));
  FocusOn(ADblPoint, mid, Viewport);
end;

procedure FocusOn(FocusPoint: TDblPoint; ScreenPoint: TPoint; Viewport: TRect);
var
  tmp: TPoint;
begin
  tmp := WorldToScreen(FocusPoint);
  XOffset += (ScreenPoint.X - tmp.x);
  YOffset += (ScreenPoint.Y - tmp.y);
end;

end.
