# AdvPaint #

Semi-vector graphical editor. Supports simple shapes (e.g. Rectangle/Square, Ellipse/Circle) and their properties. Features RTTI (to discover in runtime and  edit shape properties dynamically)

It is an academic project written for "Programming of Windowed Applications" course of FEFU.

### Dependencies ###

* Lazarus
* Free Pascal

### License ###

You're absolutely free to do anything you want with the code. Consider it as being in a public domain. However, I cannot provide you any guarantees, so be careful.

### For those who's gonna search it ###

* Б8103
* Б8103а